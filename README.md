# GLPI Docker Image

This repo contains both the Dockerfile and associated artefacts for, and the docker-compose file for using, an instance of [GLPI](https://glpi-project.org/) in Docker.

You can pull a docker image from the project's attached registry, using `docker pull registry.gitlab.com/matt.jolly/glpi-docker:latest`

You may also deploy the image from the included docker-compose file, however this requires some (basic) configuration, and assumes that you are using [Traefik](https://traefik.io/). An easily-deployed Traefik stack can be found [here](https://gitlab.com/Matt.Jolly/traefik-grafana-prometheus-docker).

## Usage

For convenience, the included `configure-compose.sh` script may be run to configure the `docker-compose.yml`.

```bash
docker stack deploy -c docker-compose.yml glpi
```

### Manual Configuration

Substitute `«DOMAIN»`,`«MARIADB_ROOT_PASSWORD»`, and `«MARIADB_ROOT_PASSWORD»` in `docker-compose.yml` to suit your environment. If you do not wish to use Traefik, you will need to expose port `80` on the container.

### GLPI Installation

Once your instance is running you will need to configure the GLPI instance using its installation wizard. This image has been configured to lock down the `/install/` subdirectory, so you will need to remove the `.htaccess` file preventing accessfrom the running container. 

```
docker container ls
...
docker container exec «container id» rm /var/www/glpi/install/.htaccess
```

Don't forget to re-deploy the stack once your installation is complete to restrict access to this directory!

### Upgrades

Just update the image version in your docker-compose file and redeploy the stack:

```bash
docker stack rm glpi
docker stack deploy -c docker-compose.yml glpi
```
