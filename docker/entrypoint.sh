#!/bin/bash

# If the files in /etc/glpi and /var/lib/glpi don't exist, copy them from the included GLPI installation; it's a fresh install!
if [ -d "/var/www/glpi/config" ]
then
	if [ "$(ls -A /var/www/glpi/config)" ]; then
     echo "Using existing configuration at /var/www/glpi/config"
	else
    echo "Populating /var/www/glpi/config with base configuration"
    cp  /root/local_define.php /var/www/glpi/config/
		chown -R www-data:www-data /var/www/glpi/config/
	fi
else
	echo "Directory /etc/glpi not found."
	exit 1
fi

if [ -d "/var/lib/glpi" ]
then
	if [ "$(ls -A /var/lib/glpi)" ]; then
     echo "Using existing data at /var/lib/glpi/"
	else
    echo "Copying files to /var/lib/glpi"
    cp -r /var/www/glpi/files/* /var/lib/glpi/
	fi 
else
	echo "Directory /var/lib/glpi not found."
	exit 1
fi

echo "Setting ownership of GLPI directories."
# Set ownership of directories for GLPI
chown -R www-data:www-data /var/lib/glpi/
chown -R www-data:www-data /etc/glpi/
chown -R www-data:www-data /var/log/glpi/

echo "Setting hostname for Apache2."
echo "ServerName ${HOSTNAME}" > /etc/apache2/conf-available/fqdn.conf

a2enconf fqdn

echo "Starting Apache2..."
apachectl -D FOREGROUND
