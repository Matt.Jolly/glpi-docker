#!/bin/bash
# Copyright (C) 2022 Matt Jolly

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# GLPI Docker stack configuration script.
# This script will use `sed` to substitute replacement variables to configure the URI
# that you want your service to run on. It will also configure the mariadb credentials.

echo "Please enter the subdomain UNDER which you want the GLPI stack to run."
echo "E.g. for 'glpi.docker.domain.tld' enter 'docker.domain.tld'"

read DOMAIN

printf "\n"
echo "Please enter the password for your mariadb 'root' user"
read -sr MARIADB_ROOT_PASSWORD
# Escape any special characters in the password
ESCAPED_MARIADB_ROOT_PASSWORD=$(sed -e 's/[$\/&]/\\&/g' <<< $MARIADB_ROOT_PASSWORD)
unset MARIADB_ROOT_PASSWORD

printf "\n"
echo "Please enter the password for your mariadb 'glpi' user"
read -sr MARIADB_GLPI_PASSWORD
ESCAPED_MARIADB_GLPI_PASSWORD=$(sed -e 's/[$\/&]/\\&/g' <<< $MARIADB_GLPI_PASSWORD)
unset MARIADB_GLPI_PASSWORD


printf "\n"
echo "Configuring your Docker Compose file."
sed -i "s/«DOMAIN»/${DOMAIN}/g" docker-compose.yml
sed -i "s/«MARIADB_ROOT_PASSWORD»/${ESCAPED_MARIADB_ROOT_PASSWORD}/g" docker-compose.yml
sed -i "s/«MARIADB_GLPI_PASSWORD»/${ESCAPED_MARIADB_ROOT_PASSWORD}/g" docker-compose.yml

unset ESCAPED_MARIADB_ROOT_PASSWORD 
unset ESCAPED_MARIADB_GLPI_PASSWORD

echo "Creating directories for GLPI bind mounts."

mkdir -p $(pwd)/mounts/{config,lib,log,mariadb}

echo "Done. You should be able to deploy the stack using:"
echo "'docker stack deploy -c docker-compose.yml glpi'"
